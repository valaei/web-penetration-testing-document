# INTRODUCTION TO PENETRATION TESTING ON WEB & WEB APPS #

This document is designed to have a general overview of top vulnerabilities on Web, and contains common industry standard tools and techniques for the purpose of penetration testing and can be quite useful for CTF contest or for CompTIA PenTest+ exam. Significant parts of this document are gathered from TryHackMe modules, OWASP, PortSwigger, GeeksForGeeks, Medium and other valuable open-source resources. 

**I designed this document for Sourced Group to be presented at the Brown Bag session for educational purposes.**

[INTRODUCTION TO PENETRATION TESTING ON WEB & WEB APPS](http://web-pentest-av.s3-website-ap-southeast-2.amazonaws.com)
